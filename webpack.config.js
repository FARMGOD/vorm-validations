var path = require('path');

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'index.js',
        library: 'vorm-validations-vuexorm-plugin',
        libraryTarget: 'umd',
        globalObject: `(typeof self !== 'undefined' ? self : this)`,
    }
};

