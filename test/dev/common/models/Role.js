import { Model } from '@vuex-orm/core';
import User from './User';

export default class Role extends Model {
    static entity = 'roles';

    static primaryKey = 'id';

    static fields() {
        return {
            id: this.increment(),
            name: this.attr(''),
            users: this.hasMany(User, 'roleId')
        };
    }
}
