import { createStore } from "../dev/";
import User from "../dev/common/models/User";
import Role from "../dev/common/models/Role";
import { expect } from "chai";

describe("Vuex ORM Validations plugin default installation", function () {
  it("should have errors object in model instances", function () {
    const store = createStore([
      {
        model: User,
      },
    ]);
    let u = new User();
    expect(u.errors).to.be.an("object");
  });

  it("should map errors from the root model", function () {
    const store = createStore([
      {
        model: User,
      },
      {
        model: Role,
      },
    ]);

    let user = new User({
      id: 1,
      roleId: 1,
    });
    let user2 = new User({
      id: 2,
      roleId: 1,
    });
    let role = new Role({
      id: 1,
    });

    Role.insert({
      data: role,
    });
    User.insert({
      data: [user, user2],
    });


    // API Call returns an invalid response such as:
    const errorData = {
      detail: [
        {
          loc: ["body", "name"],
          msg: "field required",
          type: "value_error.missing",
        },
        {
          loc: ["body", "users", 0, "email"],
          msg: "field a required",
          type: "value_error.missing",
        },
        {
          loc: ["body", "users", 1, "email"],
          msg: "field b required",
          type: "value_error.missing",
        },
      ],
    };

    // pass the response data into the model with that id
    // // set depth to 1 to filter out 'body' from the keypath
    const invalidRole = Role.find(1);
    invalidRole.validate(errorData, { depth: 1 });

    // later, in a form, load the model hierarchy with mapped errors
    const validatedRole = Role.query().with('users').find(1);

    expect(validatedRole.errors.name).to.equal('field required')
    expect(validatedRole.users[0].errors.email).to.equal('field a required')
    expect(validatedRole.users[1].errors.email).to.equal('field b required')
  });
});
