# vorm-validations

Vuex Orm Validations

### About
The use case for this plugin is to map backend validation errors to model fields in Vuex Orm models, so that they can be rendered inline in forms (such as VueFormulate).

The example below uses a FastAPI error response, like:- https://fastapi.tiangolo.com/tutorial/handling-errors/#use-the-requestvalidationerror-body - but could be adapted to parse additional formats in a future version.

### Installation
`npm install vorm-validations`

``` js
import Vue from 'vue';
import Vuex from 'vuex';
import VuexORM from '@vuex-orm/core';
import Validations from 'vorm-validations';

Vue.use(Vuex);
VuexORM.use(Validations);
```


### Usage

``` js
const store = createStore([
  {
    model: User,
  },
  {
    model: Role,
  },
]);

let user = new User({
  id: 1,
  roleId: 1,
});
let user2 = new User({
  id: 2,
  roleId: 1,
});
let role = new Role({
  id: 1,
});

Role.insert({
  data: role,
});
User.insert({
  data: [user, user2],
});


// API Call returns an invalid response such as:
const errorData = {
  detail: [
    {
      loc: ["body", "name"],
      msg: "field required",
      type: "value_error.missing",
    },
    {
      loc: ["body", "users", 0, "email"],
      msg: "field a required",
      type: "value_error.missing",
    },
    {
      loc: ["body", "users", 1, "email"],
      msg: "field b required",
      type: "value_error.missing",
    },
  ],
};

// pass the response data into the model with that id
// // set depth to 1 to filter out 'body' from the keypath
const invalidRole = Role.find(1);
invalidRole.validate(errorData, { depth: 1 });

// later, in a form, load the model hierarchy with mapped errors
const validatedRole = Role.query().with('users').find(1);

expect(validatedRole.errors.name).to.equal('field required')
expect(validatedRole.users[0].errors.email).to.equal('field a required')
expect(validatedRole.users[1].errors.email).to.equal('field b required')
```
